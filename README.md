### DSBServer
Der DSBServer besteht aus drei Teilen:

## dsbapi
Dieses Projekt ist eine reine python-Bibliothek, welche in den beiden anderen Teilen verwendet wird.
Sie ist für das Holen der Daten, Formatieren der Vertretungen etc. zuständig.

## datahandler
Der ``datahandler`` benutzt ``dsbapi`` um Daten zu bekommen und schreibt diese in Dateien, damit der ``grpcserver`` diese dem Nutzer weitergeben kann.
Er wird in regelmäßigen Abständen ausgeführt und prüft, ob neue Daten vorhanden sind, in welchem Fall er diese als push-Benachrichtigung an die Handys der Benutzer sendet.

## grpcserver
Der gRPC-Server läuft dauerhaft auf dem Pi und verarbeitet Anfragen der Clients, indem er Dateien ausliest, welche vom dritten Teil, dem ``notificationserver`` zu Verfügung gestellt werden.

Weitere Informationen sind unter ``dokumentation/facharbeit.pdf`` zu finden.
