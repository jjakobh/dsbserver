#!/usr/bin/env python3

from setuptools import setup

setup(
    name="dsbapi",
    author="FJ Tech",
    author_email="fjtech.team@gmail.com",
    version="3.5.1",
    license="MIT",
    packages=["dsbapi"],
    install_requires=["beautifulsoup4", "requests", "mashumaro", "icalendar"],
)
