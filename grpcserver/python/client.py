#!/usr/bin/env python3 -i
import grpc
import dsbapi_pb2_grpc
import dsbapi_pb2
import sys

import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--host",
    dest="host",
    default="localhost",
    help="server on which the grpc service runs",
)
parser.add_argument(
    "-p", "--port", dest="port", default=1337, help="port to run the server on"
)
parser.add_argument("login")

args = parser.parse_args()

if args.login.count(":") != 1:
    parser.print_usage()
    sys.stderr.write(
        "{}: error: login has to be formatted as 'username:password'\n".format(
            sys.argv[0]
        )
    )
    exit(1)
username, password = args.login.split(":")


# setup connection
print("Connecting to {}:{}...".format(args.host, args.port))
channel = grpc.insecure_channel("{}:{}".format(args.host, args.port))
stub = dsbapi_pb2_grpc.DSBApiStub(channel)

# create auth object
auth = dsbapi_pb2.Auth(username=username, password=password)
