import grpc

import dsbapi_pb2
import dsbapi_pb2_grpc

channel = grpc.insecure_channel("localhost:9999")
stub = dsbapi_pb2_grpc.DSBApiStub(channel)

# create auth object
auth = dsbapi_pb2.Auth()
auth.username, auth.password = "username", "password"

assert stub.GetTimetables(auth).urls == ["url1", "url2"], "urls dont match"

events = list(stub.GetEvents(auth))
assert len(events) == 2
assert events[1].time == "08:00 Uhr"

assert len(list(stub.GetNews(auth))) == 0

assert list(stub.GetTiles(auth))[0].title == "Weihnachten 2018"

print("everything went fine :)")
