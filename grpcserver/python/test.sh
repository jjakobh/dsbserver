#!/bin/bash

python3 server.py -p 9999 testdata --test > /dev/null &
pid=$!

sleep 1
python3 test.py

kill $pid
