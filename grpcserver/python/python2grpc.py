import dsbapi
import dsbapi_pb2
import datetime


def date(date: datetime.date) -> dsbapi_pb2.Date:
    if date is None:
        return None
    return dsbapi_pb2.Date(year=date.year, month=date.month, day=date.day)


def tile(tile: dsbapi.Tile) -> dsbapi_pb2.Tile:
    return dsbapi_pb2.Tile(
        title=tile.title, urls=tile.urls, date=date(tile.date.date())
    )


def news(news: dsbapi.News) -> dsbapi_pb2.News:
    return dsbapi_pb2.Tile(
        title=news.title, text=news.text, date=date(news.date.date())
    )


def substitution(s: dsbapi.Substitution) -> dsbapi_pb2.Substitution:
    return dsbapi_pb2.Substitution(
        pretty="[{}]: {}".format(", ".join(s.classes), s.pretty()),
        classes=s.classes,
        period=s.period,
        subject=s.subject_new,
        subjectOld=s.subject_old,
        teacherNew=s.teacher_new,
        teacherOld=s.teacher_old,
        roomOld="",
        room=s.room,
        type=s.type,
        text=s.text,
    )


def substitutionTable(
    substitutionTable: dsbapi.SubstitutionTable
) -> dsbapi_pb2.SubstitutionTable:
    return dsbapi_pb2.SubstitutionTable(
        date=date(substitutionTable.date),
        affected=sorted(list(substitutionTable.affected)),
        absent=sorted(list(substitutionTable.absent)),
        substitutions=[substitution(x) for x in substitutionTable.substitutions],
    )


def event(event: dsbapi.Event) -> dsbapi_pb2.Event:
    return dsbapi_pb2.Event(
        title=event.title,
        startdate=date(event.startdate),
        enddate=date(event.enddate),
        time=event.time,
        location=event.location,
    )
