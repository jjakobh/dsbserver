#!/usr/bin/env python3
import argparse
import json
import os
import signal
from concurrent import futures
import hashlib

import dsbapi
import grpc

import dsbapi_pb2
import dsbapi_pb2_grpc

import python2grpc

import logging

# CLI-Argumente
parser = argparse.ArgumentParser("server")
parser.add_argument(
    "-p", "--port", dest="port", default=1337, help="port to run the server on"
)
parser.add_argument("-v", action="store_true", help="verbose")
parser.add_argument("--test", action="store_true", help="dont check for valid login")
parser.add_argument("folder", help="folder where the data is")

args = parser.parse_args()

verbose = args.v
folder = args.folder

# logging Konfiguration
logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)


def log(auth, context, name):
    text = name
    if auth is not None:
        text += f"@{auth.username}"
    text += f" from '{dict(context.invocation_metadata())['user-agent']}'"
    logging.info(text)


def login_info(auth):
    if args.test:
        return (True, True)
    logins = [
        (
            "237222",
            "aca55d41154f799a673226d5ae68c12738ca3071516d7c76e65e95b858690847",
            False,
        ),
        (
            "237221",
            "326ffd1ab3382966cd001f8fdc01fc09735af1166513a7e1ac19c7d32e9845fd",
            True,
        ),
    ]
    for username, digest, isTeacher in logins:
        hash = hashlib.sha256(auth.password.lower().encode("utf-8")).hexdigest()
        if username == auth.username and hash == digest:
            return (True, isTeacher)
    return (False, False)


def check_login(func):
    def wrapper(self, auth, context):
        (is_valid, _is_teacher) = login_info(auth)
        if not is_valid:
            logging.warning(
                f"invalid login data: {auth.username}:{auth.password} from {dict(context.invocation_metadata())}"
            )
            return

        for v in func(self, auth, context):
            yield v

    return wrapper


class DSBAPIService(dsbapi_pb2_grpc.DSBApiServicer):
    def GetLoginInfo(self, auth, context):
        log(auth, context, "GetLoginInfo")
        (is_valid, is_teacher) = login_info(auth)
        return dsbapi_pb2.LoginInfo(isValid=is_valid, isTeacher=is_teacher)

    @check_login
    def GetSubstitutionTables(self, auth, context):
        log(auth, context, "GetSubstitutionTables")
        file = os.path.join(folder, auth.username, "substitutions.json")
        if not os.path.exists(file):
            print(f"file '{file}' not found'")
            return

        with open(file) as f:
            for s in json.load(f):
                yield python2grpc.substitutionTable(
                    dsbapi.SubstitutionTable.from_dict(s)
                )

    @check_login
    def GetNews(self, auth, context):
        log(auth, context, "GetNews")
        file = os.path.join(folder, auth.username, "news.json")
        if not os.path.exists(file):
            print(f"file '{file}' not found'")
            return

        with open(file) as f:
            for n in json.load(f):
                yield python2grpc.news(dsbapi.News.from_dict(n))

    @check_login
    def GetTiles(self, auth, context):
        log(auth, context, "GetTiles")
        file = os.path.join(folder, auth.username, "tiles.json")
        if not os.path.exists(file):
            print(f"file '{file}' not found'")
            return

        with open(file) as f:
            for t in json.load(f):
                yield python2grpc.tile(dsbapi.Tile.from_dict(t))

    def GetEvents(self, empty, context):
        log(None, context, "GetEvents")
        file = os.path.join(folder, "events.json")
        if not os.path.exists(file):
            print(f"file '{file}' not found'")
            return

        with open(file) as f:
            for e in json.load(f):
                yield python2grpc.event(dsbapi.Event.from_dict(e))

    def GetTimetables(self, auth, context):
        log(None, context, "GetTimetables")
        file = os.path.join(folder, auth.username, "urls.json")
        if not os.path.exists(file):
            print(f"file '{file}' not found'")
            return dsbapi_pb2.Timetables()
        with open(file) as f:
            return dsbapi_pb2.Timetables(urls=json.load(f))


# create a gRPC server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
dsbapi_pb2_grpc.add_DSBApiServicer_to_server(DSBAPIService(), server)


print("Starting server. Listening on port {}.".format(args.port))
server.add_insecure_port("[::]:{}".format(args.port))
server.start()


def shutdown(sig, frame):
    print("\nShutting down server...")
    server.stop(0)


# wait for Ctrl+C and then shutdown
signal.signal(signal.SIGINT, shutdown)
signal.pause()
