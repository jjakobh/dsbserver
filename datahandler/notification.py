import json
import requests
import os
from pyfcm import FCMNotification
import locale
import dsbapi
import datetime
from enum import Enum
from typing import List, Optional
import helper

# %A im Datum-Format wird 'Montag', nicht 'Monday'
locale.setlocale(locale.LC_TIME, "de_DE.utf-8")


# 5;6;7;8;9;EF;Q1;Q2
# simple5;simple6 etc
class Topic(Enum):
    TILES = "tiles"
    NEWS = "news"
    SPECIAL = "special"


### Globale- und Umgebungsvariablen und
# lese Keys aus den environment variables
FCM_KEY = os.getenv("DSBCHG_FCM_KEY")
PUSHME_KEYS = os.getenv("DSBCHG_PUSHME_KEYS", "").split(":")
PUSHME_TOPICS = os.getenv("DSBCHG_PUSHME_TOPICS", "").split(":")
PUSHME_CLASSES = [
    "simple" + x.lower()
    if os.getenv("DSBCHG_PUSHME_SIMPLE", "").lower() in ["1", "true"]
    else x.lower()
    for x in os.getenv("DSBCHG_PUSHME_CLASSES", "").split(":")
]
VERBOSE = False
FCM = False
PUSHME = False


def init(verbose=False, fcm=False, pushme=False):
    """setzt globale Optionen für das notification-Modul.
    Falls bspw. fcm=False ist wird bei einem aufruf an pushFCM
    _keine_ Benachrichtigung durchgeführt.
    """
    global VERBOSE, PUSHME, FCM
    VERBOSE = verbose
    PUSHME = pushme
    FCM = fcm


# generelle Benachrichtigung
def notify(title: str, text: str, topic: str):
    """sendet Benachrichtigungen für sowohl pushMe als auch FCM"""
    push_fcm(FCM_KEY, text, title, topic)
    if topic in PUSHME_TOPICS or topic in PUSHME_CLASSES:
        text = "{}\n{}".format(title, text)
        for key in PUSHME_KEYS:
            push_me(key, text)


### spezialisiert für dsbapi-Datentypen
def notify_sub(sub: dsbapi.Substitution, date: datetime.date):
    """notify_sub benachrichtigt alle betroffenen Stufen,
    sowohl als z.B. Topic '7' und 'simple7'.
    Der '''date'''-Parameter beschreibt das Datum des Vertretungsplans, der als
    'Montag, 21.03.2019' formatiert als Titel benutzt wird.
    """
    for c in helper.get_affected_stufen(sub.classes):
        datestr = date.strftime("%A, %d.%m.%y")
        push_fcm(FCM_KEY, f"[{','.join(sub.classes)}] {sub.pretty()}", datestr, c)
        if c.lower() in PUSHME_CLASSES:
            for key in PUSHME_KEYS:
                push_me(key, "{}: {}".format(datestr, sub.pretty()))


def notify_subs_simple(class_: str, count: int):
    notify(
        "Neue Vertretungen in der {}".format(class_),
        "+{} Vertretungen".format(count),
        "simple" + class_.lower(),
    )


def notify_tile(tile: dsbapi.Tile):
    """sende einen Aushang"""
    notify("Aushang:", tile.title, Topic.TILES.value)


def notify_news(news: dsbapi.News):
    """sende News"""
    notify(news.title, news.text, Topic.NEWS.value)


### Service implementation
def push_me(identifier: Optional[str], text: str, url=None):
    """sende eine Benachrichtigung an pushMe, wird verwendet,
    da ich die App (noch) nicht benutzen kann :(
    """
    print(f"push {text} to {identifier}")
    stop = not PUSHME
    if VERBOSE:
        print(f" - PM ({not stop}): {text}")
        if not identifier:
            print("no pushme identifier")
    if stop:
        return
    data = {"identifier": identifier, "title": text}
    if url:
        data["url"] = url
    requests.post(
        "https://pushmeapi.jagcesar.se",
        data=json.dumps(data),
        headers={"Content-Type": "application/json"},
    )


def push_fcm(key: Optional[str], text: str, title: str, topic: str):
    """sende eine Benachrichtigune via Firebase Cloud Messaging
    '''topic''' sollte entweder ein "news", "tiles", "special",
    "7" bis "Q2" oder "simple7" bis "simpleQ2" sein.
    """
    stop = not FCM or not FCM_KEY or not topic
    if VERBOSE:
        print(f" - FCM ({not stop}): {title}: {text}\t{topic}")
        if not FCM_KEY:
            print("no FCM identifier")
    if stop:
        return
    fcm = FCMNotification(api_key=key)

    fcm.notify_topic_subscribers(
        message_body=text, message_title=title, topic_name=topic
    )
