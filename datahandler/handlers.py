from typing import List, Any, Tuple, Dict
from colorama import Fore as Color
import json
import os
import dsbapi
import notification
import datetime
import helper

__all__ = ["handle", "URLHandler"]


# Referenz-Handler, quasi ein Interface
class Handler:
    name: str = "HANDLER"
    filename: str = "FILENAME"

    ignore = False

    default: Any

    @staticmethod
    def load(f):
        """liest Daten aus einer Datei"""
        pass

    def write(self, f):
        """schreibt Daten in Datei"""
        pass

    def diff(self, old):
        """gibt neues zurück, verglichen mit alten Daten"""
        pass

    def handleNew(self, diff):
        """verarbeite Neues"""
        pass


# load, write, diff, handle
def handle(handler: Handler, folder: str):
    if not handler.ignore:
        print(Color.CYAN, end="")
        print("--- {} --- {}".format(handler.name.upper(), handler.filename))
        print(Color.RESET, end="")

    # bspw 237222/substitutions.json
    file = f"{folder}/{handler.filename}"

    # lade alte daten
    if os.path.exists(file):
        with open(file, "r") as f:
            old = handler.load(f)
    else:
        old = handler.default

    # schreibe neue daten
    with open(file, "w") as f:
        handler.write(f)

    # ermittle neue daten
    diff = handler.diff(old)

    # verarbeite neue daten (benachrichtigungen, Konsolenausgaben)
    print(Color.WHITE, end="")
    handler.handleNew(diff)
    print(Color.RESET, end="")


class URLHandler(Handler):
    name = "URLs"
    filename = "urls.json"

    ignore = True

    urls: List[str]
    default: List[str] = []

    def __init__(self, urls):
        self.urls = urls

    def write(self, f):
        json.dump(self.urls, f, indent=2)


class SubstitutionHandler(Handler):
    name = "Substitutiontables"
    filename = "substitutions.json"

    timetables: List[dsbapi.SubstitutionTable]
    default: List[dsbapi.SubstitutionTable] = []

    verbose: bool

    notifications: bool

    def __init__(self, timetables, notifications=False, verbose=False):
        self.timetables = timetables
        self.notifications = notifications
        self.verbose = verbose

    @staticmethod
    def load(f) -> List[dsbapi.SubstitutionTable]:
        return [dsbapi.SubstitutionTable.from_dict(x) for x in json.load(f)]

    def write(self, f):
        json.dump(self.timetables, f, indent=2, default=lambda x: x.to_dict())

    def diff(
        self, old_timetables: List[dsbapi.SubstitutionTable]
    ) -> List[Tuple[datetime.date, List[dsbapi.Substitution]]]:
        new = []  # type: List[Tuple[datetime.date, List[dsbapi.Substitution]]]
        for new_subs_table in self.timetables:
            new_subs = []  # type: List[dsbapi.Substitution]
            for old_subs_table in old_timetables:
                if new_subs_table.date == old_subs_table.date:
                    new_subs = list(
                        set(new_subs_table.substitutions).difference(
                            old_subs_table.substitutions
                        )
                    )
                    break
            else:
                new_subs = new_subs_table.substitutions
            new.append((new_subs_table.date, new_subs))

        return new

    def handleNew(self, new: List[Tuple[datetime.date, List[dsbapi.Substitution]]]):
        affected_count = {}  # type: Dict[str, int]
        for date, subs in new:
            print(f"{len(subs)} new for {date.strftime('%d.%m.%Y')}")

            for sub in subs:
                for class_ in helper.get_affected_stufen(sub.classes):
                    if class_:
                        affected_count[class_] = affected_count.get(class_, 0) + 1
                if self.verbose:
                    print(f"  [{','.join(sub.classes)}] {sub.pretty()}")
                if self.notifications:
                    notification.notify_sub(sub, date)
        for class_ in affected_count:
            if self.verbose:
                print(" - {} new in {}".format(affected_count[class_], class_))
            if self.notifications:
                notification.notify_subs_simple(class_, affected_count[class_])


class TileHandler(Handler):
    name = "Tiles"
    filename = "tiles.json"

    tiles: List[dsbapi.Tile]
    default: List[dsbapi.Tile] = []

    notifications: bool

    def __init__(self, tiles, notifications=False):
        self.tiles = tiles
        self.notifications = notifications

    @staticmethod
    def load(f) -> List[dsbapi.Tile]:
        return [dsbapi.Tile.from_dict(x) for x in json.load(f)]

    def write(self, f):
        # formtiere die Aushänge als json, benutze zum formatieren $objekt.to_dict()
        json.dump(self.tiles, f, indent=2, default=lambda x: x.to_dict())

    def diff(self, old_tiles):
        return list(filter(lambda tile: tile not in old_tiles, self.tiles))

    def handleNew(self, new):
        if len(new) != 0:
            print("{} new tiles".format(len(new)))
        for tile in new:
            print(f" - {tile.title}")
            if self.notifications:
                notification.notify_tile(tile)


class NewsHandler(Handler):
    name = "News"
    filename = "news.json"

    news: List[dsbapi.News]
    default: List[dsbapi.News] = []

    notifications: bool

    def __init__(self, news, notifications=False):
        self.news = news
        self.notifications = notifications

    @staticmethod
    def load(f) -> List[dsbapi.News]:
        return [dsbapi.News.from_dict(x) for x in json.load(f)]

    def write(self, f):
        json.dump(self.news, f, indent=2, default=lambda x: x.to_dict())

    def diff(self, old):
        return list(filter(lambda news: news not in old, self.news))

    def handleNew(self, new):
        if len(new) != 0:
            print(f"{len(new)} new news")
        for n in new:
            print(f" - {n.title}: {n.text}")
            notification.notify_news(n)


class EventHandler(Handler):
    name = "Events"
    filename = "events.json"

    events: List[dsbapi.Event]
    default: List[dsbapi.Event] = []

    def __init__(self, events):
        self.events = [
            event for event in events if event.startdate >= datetime.date.today()
        ]

    @staticmethod
    def load(f) -> List[dsbapi.Tile]:
        return [dsbapi.Event.from_dict(x) for x in json.load(f)]

    def write(self, f):
        json.dump(self.events, f, indent=2, default=lambda x: x.to_dict())

    def diff(self, old):
        return list(filter(lambda event: event not in old, self.events))

    def handleNew(self, new):
        if len(new) != 0:
            print(f"{len(new)} new events")
        for n in new:
            pass  # print(n)
