import sys
import os
from typing import List, Tuple


def error(msg: str):
    """schreibe Fehler an stderr und beende"""
    sys.stderr.write("{}: error: {}'\n".format(sys.argv[0], msg))
    exit(1)


def create_dirs(folder: str):
    """stelle sicher, dass Ordner existieren, ansonsten erstelle sie"""
    if not os.path.exists(folder):
        try:
            os.makedirs(folder)
        except PermissionError as e:
            error("Permission denied: could not create folder {}".format(e.filename))


def get_affected_stufen(classes: Tuple[str]) -> List[str]:
    """Diese Funktion filtert aus einem Array aus Klassen
    alle betroffenen Stufen heraus.
    Beispiel:
    ["7A", "7B", "7C", "EF", "Q1"] -> ["7", "EF", "Q1"]
    """
    # ersetze EF mit '*' und SE mit '+'
    affected = [x.replace("EF", "*").replace("SE", "+") for x in classes]
    # lösche A,B,C,D,E,F,0 nicht benötigt
    affected = [
        x.replace("A", "")
        .replace("B", "")
        .replace("C", "")
        .replace("D", "")
        .replace("E", "")
        .replace("F", "")
        .replace("0", "")
        for x in affected
    ]
    # entferne doppelte
    affectedset = set(affected)
    # wieder '+' zu SE und '*' zu EF
    affected = [x.replace("*", "EF").replace("+", "SE") for x in affectedset]
    # sortieren
    affected.sort()
    return affected
