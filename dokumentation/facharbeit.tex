\documentclass[12pt,a4paper,oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}

% === PAKETE === 
\usepackage{fancyhdr}									% header/footer
\usepackage{graphicx}									% bilder
\usepackage[T1]{fontenc}								% deutsche quotes
\usepackage[headheight=20pt]{geometry}	% für seitenränder
\usepackage{acronym}									% abkürzungsverzeichnis
\usepackage{minted}										% syntax
\usepackage[simplified]{pgf-umlcd}				% UML
\usepackage[most]{tcolorbox}						% box für minted\right 
\usepackage{hyperref}									% links
\usepackage{verbatim}


% === KONFIGURATION ===
% minted
\usemintedstyle{xcode}
\BeforeBeginEnvironment{minted}{\begin{tcolorbox}[breakable]}
\AfterEndEnvironment{minted}{\end{tcolorbox}}

\geometry{left=3cm,right=2cm,top=4cm,bottom=4cm}
\linespread{1.3} % anderthalb-zeilig
\setlength{\parindent}{0cm}

\bibliographystyle{unsrt} % literaturverzeichnis

\makeatletter
\renewcommand{\@dotsep}{10000} % keine punkte im inhaltsverzeichnis
\makeatother

% alias
\newcommand{\code}{\texttt}

% ==================== DOKUMENT ====================
\begin{document}
% ==================== TITLEPAGE ====================
\begin{titlepage}
\newgeometry{} % keine besonderen seitenränder
% fancyhdr
\thispagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\lfoot{vorgelegt von \\Johann Jakob Hellermann}
\cfoot{Q1}
\rfoot{Schuljahr 2018/19}

\center \includegraphics[width=10cm]{assets/dsbchg_logo.png}
\medbreak
\center \begin{huge}DSBCHG\end{huge}

\end{titlepage}

% ==================== INHALTSVERZEICHNIS ====================
\thispagestyle{empty}
\tableofcontents
\newpage

% ==================== INHALT  ====================

% seitenzahlen
\setcounter{page}{3}
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\chead{\thepage}

% ==================== Vorwort ====================
\section{Vorwort}
\paragraph{Wieso?}
Das \ac{CHG} benutzt das Digitale Schwarze Brett (DSB), um Vertretungen auf einem Monitor in der Schule anzuzeigen und diese sowie Aushänge den Schülern über eine App zugänglich zu machen. \\
Es gibt jedoch einige Probleme mit dieser App.
Zum Einen ist sie alles andere als schön. \\
Zum Anderen fehlen einige sinnvolle Features, die als Schüler oder auch als Lehrer sehr praktisch wären.
Dazu zählen Benachrichtigungen, Filterung nach relevanten Informationen und die Möglichkeit, weitere Daten des \ac{CHG} (Termine, Klausurpläne etc.) anzuzeigen. \\
Aus diesem Grund beschlossen ein Mitschüler (Furkan Gedikoglu) und ich eine eigene App zu programmieren, mit der wir all diese Probleme beseitigen würden.

\vspace{2cm}
% ==================== DSBCHG ====================
\section{Funktionsweise}
DSBCHG ist als Client/Server-Modell strukturiert. \\
Das bedeutet, dass nicht jedes Handy für sich seine Daten von \ac{DSB} holt, diese verarbeitet und anzeigt, sondern dass ein zentraler Server (in unserem Fall ein Raspberry Pi) die netzwerk- und ressourcenintensive Arbeit ausführt und den Clients, den Benutzern der App, die Daten zur Verfügung stellt.
% ==================== Abfrage der Daten ====================
\subsection{Abfrage der Daten}
Zunächst erkläre ich, wie man an die benötigten Daten kommt.
Dies geschieht in zwei Schritten:
\begin{enumerate}
\label{sec:abfrage}
\item Anfrage an \ac{DSB} \\
Die Schnittstelle zum \ac{DSB}-Server verlangt, dass Benutzername und Passwort als \code{gzip} komprimiertes \code{JSON-Objekt} an die URL \\ \code{http://www.dsbmobile.de/JsonHandlerWeb.ashx/GetData} geschickt werden:

\begin{minted}[tabsize=4]{python}
# ein JSON-Objekt der Anfrage wird erstellt
creds = '{{"UserId": "{}", "UserPw": "{}", BundleId:""}}'
	.format(username, password)
encodedCreds = base64.b64encode(gzip.compress(creds.encode()))
	.decode()
request = '{"req":{"Data":"'+encodedCreds+'","DataType":1}}'
# die Anfrage wird ausgeführt
r = requests.post(
	"http://www.dsbmobile.de/JsonHandlerWeb.ashx/GetData",
	data=request, headers={...)
	)
# die Antwort wird verarbeitet und dekodiert.
# r.content == {"d":"YmFzZTY0IGVuY29kaWVydGVyIFRleHQK"}
d = json.loads(r.content.decode())['d']
response = json.loads(gzip.decompress(base64.b64decode(d))
	.decode())
\end{minted}

\code{response} beinhaltet nun ein \code{JSON-Objekt}, welches Informationen über Vertretungspläne, Aushänge und News beinhaltet.
Nimmt man all die nicht benötigte Information weg kann ein Vertretungsplan bspw. so aussehen:
\begin{minted}[tabsize=4]{json}
{
	"Id": "4828917c-0c35-47e2-8170-60a0efe76e58",
	"Date": "04.12.201811:41",
	"Title": "VP_SuS_morgen_HG",
	"Detail": "https://app.dsbcontrol.de/data/5a04...e76e58.html",
}
\end{minted}

\label{sec:parsen}
\item Verarbeiten der HTML-Tabelle \\
Öffnet man die URL, die im \code{Detail}-Feld der Daten steht, findet man die Website, welche auf dem Monitor in der Schule bzw. in der App DSBMobile angezeigt wird.
Im Quelltext der Website findet man folgende Tabelle, welche nun zuerst in \code{JSON} umgewandelt wird.
\begin{minted}[tabsize=4]{html}
<table cellspacing="1">
	<tr>
		<td>Q1</td><td>3-4</td><td>IF L1</td>
		<td>+</td><td>ZG</td><td></td><td></td>
		<td></td><td>Vertretung</td><td></td>
	</tr>
	...
</table>
\end{minted}
Jede Spalte (\code{tr}) wird zu einem verständlicheren \code{JSON-Object} umgewandelt:

\begin{minted}[tabsize=4]{json}
{
	"classes": ["Q1"], "period": "3-4",
	"subject": "", "subjectOld": "IF L1",
	"teacherNew": "+", "teacherOld": "ZG",
	"room": "", "type": "Vertretung", "text": ""
}
\end{minted}

Dies kann nach einer Reihe von Regeln zu folgender Nachricht formatiert werden: \\
\textbf{[Q1]: 3-4. Stunde IF L1 bei ZG entfällt}
\end{enumerate}

\newpage
% ==================== Struktur des Servers ====================
\section{Der Server}
Die Server-Seite von DSBCHG ist in drei Teile aufgeilt: 
\begin{enumerate}
\item \code{dsbapi} \\
Dieses Projekt ist eine reine Python-Bibliothek, welche in den beiden anderen Teilen verwendet wird.\\
Sie ist für das Beschaffen der Daten, Formatieren der Vertretungen etc. zuständig.

\item \code{datahandler} \\
Der \code{datahandler} verwendet \code{dsbapi} um an Daten zu gelangen und schreibt diese in Dateien, damit der \code{grpcserver} diese dem Nutzer weitergeben kann.\\
Er wird in regelmäßigen Abständen ausgeführt und prüft, ob neue Daten vorhanden sind, in welchem Fall er diese als push-Benachrichtigung an die Handys der Benutzer sendet.

\item \code{grpcserver} \\
Der gRPC-Server läuft dauerhaft auf dem Pi und verarbeitet Anfragen der Clients, indem er Dateien ausliest, welche vom zweiten Teil, dem \code{notificationserver}, zur Verfügung gestellt werden.
\end{enumerate}

Jeder dieser Teile ist unter \url{https://gitlab.com/jjakobh/dsbserver} öffentlich zugänglich in einem gleichnamigen Unterordner zu finden.

\newpage
% ==================== dsbapi ====================
\subsection{dsbapi}
Die Python-Bibliothek \code{dsbapi} beinhaltet folgende Klassen:
\bigbreak
\begin{tikzpicture}
\begin{package}{dsbapi}
  \begin{class}[text width=6cm]{SubstitutionTable}{0,0}
	\attribute{date : str}
	\attribute{affected : str[]}
	\attribute{absent: str[]}
	\attribute{substitutions: Substitution[]}
  \end{class}
  \begin{class}[text width=4cm]{Event}{6,0}
	\attribute{title : str}
	\attribute{startdate : datetime.datetime}
	\attribute{startdate : datetime.datetime}
	\attribute{time : str}
	\attribute{location : str}
  \end{class}
  \begin{class}[text width=4cm]{Substitution}{-4,-4}
	\attribute{classes : str[]}
	\attribute{period : str}
	\attribute{subject\_new: str}
	\attribute{subject\_old : str}
	\attribute{teacher\_new : str}
	\attribute{teacher\_old : str}
	\attribute{room : str}
	\attribute{type : str}
	\attribute{text : str}
  \end{class}
  \begin{class}[text width=5cm]{News}{2,-5.5}
	\attribute{title : str}
	\attribute{date : datetime.datetime}
	\attribute{text : str}
  \end{class}
  \begin{class}[text width=5cm]{Tile}{2,-9}
	\attribute{title : str}
	\attribute{date : datetime.datetime}
	\attribute{urls : str[]}
  \end{class}
\end{package}
\end{tikzpicture}

Die Vertretungspläne, welche im Internet zu finden sind, werden in der Bibliothek als \code{SubstitutionTable} dargestellt.\\
Dieser beinhaltet Informationen wie das Datum, abwesende, sowie betroffene Klassen, sowie die Vertretungen an sich, welche als \code{Substitution} repräsentiert werden. \\
\code{News} und \code{Tile} sind weitere Elemente des Digitalen Schwarzen Brettes: Kurznachrichten und Aushänge. \\
Die Klasse \code{Event} beschreibt einen Schultermin, wie er unter \url{http://carl-humann.de/leben-am-chg/aktuelle-termine/} zu finden ist.
\newpage
Zusätzlich werden drei Methoden exportiert: \\
\code{get\_DSB\_info(username, password) -> (str[], News[], Tiles[])}, \\
\code{substitutiontables\_from\_urls(urls) -> SubstitutionTable[]} und \\
\code{parse\_events() -> Event[]}.
\bigbreak
Die Funktionsweise von \code{get\_DSB\_info} wird in \textbf{\hyperref[sec:abfrage]{2.1.1}} genauer beschrieben und dient als Schnittstelle zwischen DSB und dem Python-Programm.
\medbreak
\code{substitutiontables\_from\_urls} wird benötigt, da DSB nur die Information bereitstellt, auf welchen URLs die Vertretungspläne zu finden sind und diese noch in Python-Objekte übersetzt werden müssen. Siehe \textbf{\hyperref[sec:parsen]{2.1.2}}.
\medbreak
\code{parse\_events} liest \url{http://carl-humann.de/leben-am-chg/aktuelle-termine} \\ und übersetzt diese in \code{Event}-Objekte.
\medbreak
\code{News}, \code{Tiles} und \code{get\_DSB\_info} sind in der Datei \code{dsb.py} zu finden, \code{SubstitutionTable}, \code{Substitution} und \code{substitutiontable\_from\_urls} in \code{substitutions.py} und \code{Event} und \code{parse\_events} in \code{event.py}
\medbreak
Die Benutzung von \code{dsbapi} kann in etwa so aussehen:
\begin{minted}[tabsize=4]{python}
# Auslesung der Daten
timetable_urls, news, tiles = dsbapi.get_DSB_info(username, password)
# Verarbeitung der Vertretungspläne
subs_tables = dsbapi.substitutiontables_from_urls(timetable_urls)

for substitution_table in subs_tables:
	print(substitution_table.date)
	print(substitution_table.substitutions)

for event in dsbapi.parse_events():
	print(event)
\end{minted}

\newpage
\subsection{datahandler}
\includegraphics[width=16cm]{assets/datahandler.png}

Das Command-Line Programm \code{datahandler} benutzt die \code{dsbapi}-Bibliothek, um Vertretungen, Aushänge etc. herunterzuladen, sie mit vorhandenen alten Daten abzugleichen und bei Bedarf neue Inhalte an Clients per push-Notification zu verschicken.
\bigbreak
Das Programm führt eine Liste von \code{handler}n aus, welche von der Klasse \code{Handler} erben:
\begin{minted}[tabsize=4]{python}
class Handler:
	name: str = "HANDLER"
	filename: str = "FILENAME"
	default: Any
	@staticmethod def load(f): pass
	def write(self, f): pass
	def diff(self, old): pass
	def handleNew(self, diff): pass
\end{minted}
Jeder Handler schreibt seine Daten \code{JSON}-formatiert in \code{data/\$username/\$handlername}.
Diesen Ordner kann später der \code{grpcserver} auslesen, um die Daten an Clients zu schicken.
\smallbreak
Das Ausführen der Handler geschieht in vier Schritten:
\begin{enumerate}
\vspace{-3mm}
\item Lesen der alten Daten
\vspace{-3mm}
\item Schreiben der neuen Daten
\vspace{-3mm}
\item Vergleichen, was an neuen Inhalten dazu gekommen ist
\vspace{-3mm}
\item Verarbeiten, d.h. Versenden von Benachrichtigungen etc.
\end{enumerate}

\begin{minted}[tabsize=4]{python}
# load, write, diff, handle
def handle(handler: Handler, folder: str):
	# bspw 237222/substitutions.json
	file = f"{folder}/{handler.filename}"
	# lade alte daten
	if os.path.exists(file):
		with open(file, "r") as f:
			old = handler.load(f)
	else:
		# keine alten Daten, falle zurück auf Standardwert
		old = handler.default
	# schreibe neue daten
	with open(file, "w") as f:
		handler.write(f)
	diff = handler.diff(old) # ermittle neue daten
	handler.handleNew(diff)  # verarbeite neue daten
\end{minted}
Implementationen dieses Handlers gibt es für URLs (\code{URLHandler}), \\
Vertretungen (\code{SubstitutionHandler}), News ({\code{NewsHandler}), Aushänge (\code{TileHandler}) und Termine (\code{EventHandler}). \\
Diese Klassen sind in \code{datahandler/handlers.py} zu finden. \\
In \code{datahandler/datahandler} wird die Liste aus Handlern ausgeführt. \\
\begin{minted}[tabsize=2]{python}
for username, password in args.login.split(':'):
	timetable_urls, news, tiles = dsbapi.get_DSB_info(username, password)
	handlers= [
		NewsHandler(news, notifications=args.news),
		TileHandler(tiles, notifications=args.tiles),
		...
	]
	for handler in handlers:
		handlers.handle(handler, folder)
\end{minted}

Als Beispiel sieht die Implementation des Handlers für Aushänge folgendermaßen aus:
\begin{minted}[tabsize=4]{python}
class TileHandler(Handler):
	tiles: List[dsbapi.Tile]
	default: List[dsbapi.Tile] = []

	def __init__(self, tiles, notifications=False):
		self.tiles = tiles
		self.notifications = notifications

	@staticmethod
	def load(f) -> List[dsbapi.Tile]:
		return [dsbapi.Tile.from_dict(x) for x in json.load(f)]

	def write(self, f):
		json.dump(
			self.tiles, f, indent=2, default=lambda x: x.to_dict()
		)

	def diff(self, old_tiles):
		tiles = []
		for t in self.tiles:
			if not t in old_tiles:
				tiles.append(t)
		return tiles

	def handleNew(self, new):
		if len(new) != 0:
			print("{} new tiles".format(len(new)))
		for tile in new:
			print(f" - {tile.title}")
			if self.notifications:
				notification.notify_tile(tile)
\end{minted}

\newpage
\subsection{grpcserver}
Um die im Datahandler verarbeiteten Daten an Clients zu Verfügung zu stellen, benutzen wir gRPC, ein Framework, welches es ermöglicht, Plattform- und Programmiersprachenübergreifende APIs zu erstellen.
\begin{figure} [ht]
  \centering
  \includegraphics[height=5cm]{assets/grpc_overview.jpg} \\
  \textsf{https://grpc.io/img/landing-2.svg}
\end{figure} \\
In unserem Fall ist der Server in python geschrieben, und Clients sind sowohl die Android-App über Java sowie (zukünftig) die iOS-App via Swift. \\
Ein gRPC-Server wird über eine \code{.proto}-Datei definiert, und wird durch eine Reihe von Services und Messages beschrieben. \\
Hier ein Beispiel für eine solche Service-Definition: \\
\begin{minted}[fontsize=\small]{proto}
service DSBApi {
  rpc getTiles (Auth) returns (stream Tile) {} // quasi eine Methode
}
message Auth {
	string password = 1;
	string username = 2;
}
message Tile {
	string title = 2;
	repeated string urls = 3;
	Date date = 4;
}
\end{minted}
\newpage
Aus dieser Datei kann nun Server/Client Code für Python, Java sowie 9\footnote{https://grpc.io/docs/reference/} weitere Sprachen generiert werden. \\
Diese Architektur ermöglicht es uns, rückwärts-kompatible Änderungen vorzunehmen, da bspw. ein zusätzlicher Service oder ein weiteres Feld in der \code{Auth}-Message hinzugefügt werden kann und alte sowie neue Clients immer noch mit dem Server kommunizieren können. \\
Wie genau diese Nachrichten Client-seitig verarbeitet werden, wird  in Furkan Gedikoglus Facharbeit\footnote{https://gitlab.com/LoreTV/schule/blob/master/Facharbeit\_Info.pdf} näher beschrieben. \\
Der \code{grpcserver} ist ein python-Programm, welches auf dem Port 1337 auf Anfragen von gRPC-Clients wartet, die Daten des \code{notificationserver}s ausliest und diese dann zurücksendet. \\
Für Aushänge sieht die Routine folgendermaßen aus: \\
\begin{minted}[tabsize=4]{python}
# dsbapi_pb2_grpc ist ein aus der .proto Datei generierter Skript
class DSBAPIService(dsbapi_pb2_grpc.DSBApiServicer):
	...
	@check_login
	def GetTiles(self, auth, context):
		file = os.path.join(folder, auth.username, "tiles.json")
		log(auth, context, "GetTiles")
		if not os.path.exists(file): # Datei existiert nicht
			print(f"file '{file}' not found'")
			return

		# öffne 'file', wird nach with-Kontext geschlossen
		with open(file) as f:
			for t in json.load(f):
				# yield == schicke ein neues Element über den Stream
				yield python2grpc.tile(dsbapi.Tile.from_dict(t))
	...
\end{minted}
\subsubsection{Sicherheit}
Jeder gRPC-Endpunkt, welcher sensitive Daten zu Verfügung stellt, wird von dem Dekorator \code{check\_login} umschlossen. \\
Ein Dekorator ermöglicht es, mehrere Methoden um Logik zu erweitern, ohne dabei redundant zu sein. Dabei wird eine Funktion definiert, welche eine zweite Funktion als Parameter nimmt und eine Dritte zurückgibt. \\
\begin{minted}[tabsize=2]{python}
def check_login(func):
	def wrapper(self, auth, context):
		(is_valid, _is_teacher) = login_info(auth)
		if not is_valid:
			logging.warning(f"invalid login data: {...}")
			return 
		for v in func(self, auth, context):
			yield v
	
	return wrapper
\end{minted}
Somit werden falsche Logindaten nicht \textit{durchgelassen}, da der Dekorator frühzeitig beendet wird.
\bigbreak
Damit keine Logindaten auf einem öffentlichen Github-Repository zugänglich sind, werden sie überprüft, indem sie gegen eine Liste von Benutzername, sha256\footnote{https://en.wikipedia.org/wiki/SHA-2}-Hash des Passworts und der Information, ob der Account Lehrer-Berechtigungen hat verglichen wird. \\
\begin{minted}[tabsize=4]{python}
def login_info(auth):
	logins = [
		("237222","aca5 ... 858690847", False),
		("237221", "326f ... 32e9845fd", True)
	]
	for username, digest, isTeacher in logins:
		hash = hashlib.sha256(auth.password.lower().encode("utf-8"))
			.hexdigest()
		if username == auth.username and hash == digest:
			return (True, isTeacher)
	return (False, False)
\end{minted}

\section{Schlusswort}
Rückblickend lässt sich sagen, dass die Entscheidung, den Server in verschiedene unabhängige Teile aufzuteilen sehr vorteilhaft war.
Dadurch wird einerseits die Problemsuche beschleunigt, da man in konkreteren Bereichen nach welchen suchen kann, während bei einer monolithischen Architektur einzelne Komponenten viel verworrener sind.\\
Python als Programmiersprache war mit einigen Vorteilen sowie einigen Nachteilen verbunden.
Die Sprache ist aufgrund ihrer dynamischen Natur gut geeignet, um einen funktionieren Prototypen zu erstellen. Dieser jedoch ist fehleranfälliger als ein, in anderen, statischen Sprachen geschriebener Code, da viele Fehler bereits während der Kompilierung auffallen.
\bigbreak
\code{gRPC} war zwar zu Beginn zwar ein großer Aufwand, welcher sich jedoch im Laufe der Zeit ausgezahlt hat. Da \code{gRPC} sehr auf rückwärts-Kompatibilität ausgelegt ist, ist es einfach, den Server um neue Funktionen zu erweitern ohne ein Problem mit veralteten Clients zu verursachen.
\bigbreak
Im Zuge dieses Projektes habe ich viel gelernt, von Implementierung in Python über Architektur von Software bis zu Problembehandlung.\\

\newpage

% ==================== Setup ====================
\section{Setup}

\subsection{Installation}
Zunächst wird das git-Projekt geklont.
\begin{verbatim}
$ git clone https://gitlab.com/jjakobh/dsbserver.git
$ cd dsbserver
\end{verbatim}
Als nächstes werden die benötigten Python-Abhängigkeiten installiert. \\
\code{dsbapi} muss nicht extra installiert werden, da es bereits in \code{requirements.txt} beinhaltet ist.
\begin{verbatim}
$ pip3 install -r datahandler/requirements.txt # beinhaltet dsbapi
$ pip3 install -r grpcserver/requirements.txt
\end{verbatim}

\begin{verbatim}
$ cd grpcserver
$ make proto
\end{verbatim}

\subsection{Einrichtung der Dienste}
Nun sollte die Datei \code{dsbserver/notificationserver/run.sh} erstellt werden, welche in etwa so aussehen muss:
\begin{verbatim}
export DSBCHG_FCM_KEY="Aox8zcCS...SpG"
# benutze 237222 für news, events und tiles
./datahandler 237222:pwd --fcm --news --events --tiles
# 237221 für Vertretungen, da hier der dritte Tag vorhanden ist.
./datahandler 237221:pwd --fcm --subs
\end{verbatim}
Den FCM-Key finden Sie unter \code{https://console.firebase.google.com} \\ -> Projekteinstellungen -> Cloud Messaging -> Serverschlüssel. \\
Diese \code{run.sh}-Datei muss nun in einem regelmäßigen Intervall ausgeführt werden. Machen Sie sie dafür zunächst ausführbar: \code{chmod +x run.sh}.
Geben sie nun \code{EDITOR=nano crontab -e} ein. 
Fügen sie die Zeile \\
\code{*/10 * * * * /home/pi/dsbserver/datahandler/run.sh} hinzu und drücken sie \\ \code{Ctrl-X Yes Enter} um den Editor zu schließen.

\newpage

% ==================== Abkürzungsverzeichnis ====================
\section{Abkürzungsverzeichnis}
\begin{acronym}[FCM]
\acro{CHG}{Carl Humann Gymnasium}
\acro{DSB}{Digitales Schwarzes Brett}
\acro{FCM}{Firebase Cloud Messaging}
\end{acronym}

% ==================== Selbtständigkeitserklärung ====================
\section{Selbstständigkeitserklärung}
\begin{quote}
Ich versichere hiermit, dass ich diese Arbeit selbstständig angefertigt und keine anderen als die von mir angegebenen Quellen und Hilfsmittel verwendet habe. Dir den benutzten Werken wörtlich oder inhaltlich entnommenen Stellen sind als solche gekennzeichnet.
\end{quote}
Ort, Datum, Unterschrift: $\rule{8cm}{0.15mm}$


\section{Anhang}
Link zum Gitlab-Projekt: \url{https://gitlab.com/jjakobh/dsbserver}
\iffalse
\subsection*{dsbapi}
\begin{verbatim}
dsbapi/dsbapi/__init__.py
dsbapi/dsbapi/events.py
dsbapi/dsbapi/dsb.py
dsbapi/dsbapi/substitutions.py
\end{verbatim}

\begin{footnotesize}
\verbatiminput{../dsbapi/dsbapi/dsb.py}
\end{footnotesize}

\begin{footnotesize}
\verbatiminput{../dsbapi/dsbapi/substitutions.py}
\end{footnotesize}

\begin{footnotesize}
\verbatiminput{../dsbapi/dsbapi/events.py}
\end{footnotesize}

\subsection*{datahandler}
\begin{verbatim}
datahandler/notification.py
datahandler/handlers.py
datahandler/helper.py
datahandler/datahandler
\end{verbatim}

\begin{footnotesize}
\verbatiminput{../datahandler/notification.py}
\end{footnotesize}

\begin{footnotesize}
\verbatiminput{../datahandler/handlers.py}
\end{footnotesize}

\begin{footnotesize}
\verbatiminput{../datahandler/helper.py}
\end{footnotesize}

\begin{footnotesize}
\verbatiminput{../datahandler/datahandler}
\end{footnotesize}

\subsection*{grpcserver}
\begin{verbatim}
grpcserver/dsbapi.proto
grpcserver/python/server.py
grpcserver/python/python2grpc.py
\end{verbatim}

\begin{footnotesize}
\verbatiminput{../grpcserver/dsbapi.proto}
\end{footnotesize}

\begin{footnotesize}
\verbatiminput{../grpcserver/python/server.py}
\end{footnotesize}

\begin{footnotesize}
\verbatiminput{../grpcserver/python/python2grpc.py}
\end{footnotesize}
\fi

\end{document}
